package horadosistema;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Date;
import java.util.Locale;

public class HoraDoSistema {

    public static void main(String[] args) {
        Date data = new Date();
        System.out.println("A hora do sistema é:");
        System.out.println(data.toString());
        
        Locale locale = Locale.getDefault();  
        System.out.print ("A língua do seu sistema é: " + locale.getDisplayLanguage()); // imprime "Português" 
        System.out.println ("  " + locale.getLanguage()); // imprime "pt"
        
        Toolkit tk = Toolkit.getDefaultToolkit();  
        Dimension d = tk.getScreenSize();  
        System.out.println("A resolução do seu sistema é: " + d.width +" X " + d.height);  
    }

}
